package gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EventObject;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Position;
import javax.xml.bind.JAXBException;

import edu.unb.fga.dadosabertos.Camara;
import edu.unb.fga.dadosabertos.Deputado;

public class Interfac extends JFrame{
	
	
	final JTextField pesquisa = new JTextField(15);
	final JTextField pesquisa2 = new JTextField(15);
	JLabel rotulo; 
	JTextField contagem;
	JTextField contagemP;
	JButton botaoContadorP;
	JButton botaoContador;
	JButton informacoes;
	JList lista;
	JList lista2;
	JList listainformacoes;
	DefaultListModel listaDeputados;
	DefaultListModel listaPartido;
	JScrollPane scroll;
	JScrollPane scroll2;
	
	EventObject e = null;
	ArrayList linhainformacoes = new ArrayList();
	
	String[] conteudoBox1 = {"Suplente"};
	String[] conteudoBox2 = {"PT","PDT","PROS","PMDB","PCdoB","PSD","PT","PSDB","PR","PTdoB","REDE","PSOL","PSDC",
			"DEM","PSB","PTB","PEN","PP","PRB","SD","PPS","PHS","PSC","PMN","S.PART","PTN","PSL","PV"};
	JComboBox filtragem;
	JComboBox conteudoPartidos; 
	
	
	public Interfac() throws JAXBException, IOException{
		super("Informações dos Deputados");
		
		criarInterface();
		
	}
	

	private void criarInterface() throws JAXBException, IOException{
		
		Camara camara = new Camara();
		camara.obterDados();
		List<Deputado> deputados = camara.getDeputados();
		
		listaDeputados = new DefaultListModel();
		for(int i = 0; i<deputados.size(); i++){
			Deputado deputado = deputados.get(i);
			listaDeputados.addElement(deputado.getNome());
		}
		
		listaPartido = new DefaultListModel();
		for(int i = 0; i<deputados.size(); i++){
			Deputado deputado = deputados.get(i);
			listaPartido.addElement(deputado.getNome());
		}
		
		ResultadoAction resultadoAction = new ResultadoAction();
		PesquisaAction pesquisaAction = new PesquisaAction();
		OrdenarAction ordenarAction = new OrdenarAction();
		FiltragemAction filtragemAction = new FiltragemAction();
		InformacoesAction informacoesAction = new InformacoesAction();
		
		//InformacoesAction informacoesAction = new InformacoesAction();
		
		
		//Cria o painel onde vao ficar as abas
		JTabbedPane abaDeputado = new JTabbedPane();
		
		//Painel de Deputados
		JPanel Deputado = new JPanel();
		Deputado.setLayout(new BorderLayout());
		
		JPanel panelBusca = new JPanel();
		panelBusca.setLayout(new FlowLayout());
		
		rotulo = new JLabel("Pesquisar na lista:");    
		panelBusca.add(rotulo);
		rotulo.setBounds(10, 5, 200, 20);
		pesquisa.addActionListener(pesquisaAction);
		pesquisa.addActionListener(ordenarAction);
		
	    panelBusca.add(rotulo);
	    panelBusca.add(pesquisa);
		
		lista = new JList(listaDeputados);
		scroll = new JScrollPane(lista);
		
		JPanel panelContador = new JPanel();
		panelContador.setLayout(new FlowLayout());
		
		contagem = new JTextField(4);
		contagem.addActionListener(resultadoAction);
		
		botaoContador = new JButton("Contador");
		botaoContador.addActionListener(resultadoAction);
		
		panelContador.add(contagem);
		panelContador.add(botaoContador);
		
		
		JPanel panelFiltragem = new JPanel();
		panelFiltragem.setLayout(new FlowLayout());
	
		 
		filtragem = new JComboBox(conteudoBox1);
		filtragem.addActionListener(pesquisaAction);
		panelFiltragem.add(filtragem);
		filtragem.addActionListener(filtragemAction);
		
		
		JPanel panelInformacoes = new JPanel();
		
		informacoes = new JButton("Ver Informações");
		panelInformacoes.add(informacoes);
		informacoes.addActionListener(informacoesAction);
		
		//Deputado.add(panelBotoes, BorderLayout.NORTH);
		Deputado.add(panelBusca, BorderLayout.CENTER);
		Deputado.add(scroll, BorderLayout.EAST);
		Deputado.add(panelContador, BorderLayout.SOUTH);
		Deputado.add(panelFiltragem, BorderLayout.WEST);
		Deputado.add(panelInformacoes, BorderLayout.NORTH);
		
		//Aba de Partidos
		JPanel Partido = new JPanel();
		Partido.setLayout(new BorderLayout());
		
		JPanel panelBotoes = new JPanel();
		panelBotoes.setLayout(new FlowLayout()); 
		
		JButton botaoInformacoes = new JButton("Ver Informações");
		botaoInformacoes.addActionListener(informacoesAction);
		panelBotoes.add(botaoInformacoes);
		
		JPanel panelBuscaP = new JPanel();
		panelBuscaP.setLayout(new FlowLayout());
		
		rotulo = new JLabel("Pesquisar na lista:");    
		panelBusca.add(rotulo);
		rotulo.setBounds(10, 5, 200, 20);
		pesquisa2.addActionListener(pesquisaAction);
		pesquisa2.addActionListener(ordenarAction);
		
	    panelBuscaP.add(rotulo);
	    panelBuscaP.add(pesquisa2);
		
		lista2 = new JList(listaPartido);
		scroll = new JScrollPane(lista2);
		
		JPanel panelContadorP = new JPanel();
		panelContadorP.setLayout(new FlowLayout());
		
		contagemP = new JTextField(4);
		contagemP.addActionListener(resultadoAction);
		
		botaoContadorP = new JButton("Contador");
		botaoContadorP.addActionListener(resultadoAction);
		
		panelContadorP.add(contagemP);
		panelContadorP.add(botaoContadorP);
		
		
		JPanel panelFiltragemP = new JPanel();
		panelFiltragemP.setLayout(new FlowLayout());
	
		JComboBox filtragem2 = new JComboBox(conteudoBox2);
		filtragem2.addActionListener(pesquisaAction);
		panelFiltragemP.add(filtragem2);
		
		//JPanel panelInformacoesP = new JPanel();
		
		//informacoes = new JButton("Ver Informações");
		//panelInformacoesP.add(informacoes);
		//informacoes.addActionListener(informacoesAction);
		
		//Deputado.add(panelInformacoes, BorderLayout.NORTH);
		Partido.add(panelBuscaP, BorderLayout.CENTER);
		Partido.add(scroll, BorderLayout.EAST);
		Partido.add(panelContadorP, BorderLayout.SOUTH);
		Partido.add(panelFiltragemP, BorderLayout.WEST);
		
		//adiciona as abas
		abaDeputado.add("Deputado", Deputado);
		abaDeputado.add("Partido", Partido);
		
		//adiciona o Tabbed Pane
		add(abaDeputado);
	}
	
	private class ResultadoAction implements ActionListener{
		
		public void actionPerformed(ActionEvent event){
				
			contagem.setText(""+listaDeputados.getSize());
			contagemP.setText(""+listaPartido.getSize());
		}
    } 
	
	private class PesquisaAction implements ActionListener{
		
		public void actionPerformed(ActionEvent event){
		
			pesquisa.getDocument().addDocumentListener(
			new DocumentListener(){
		        public void insertUpdate(DocumentEvent e){
		          pesquisarLista(pesquisa.getText()); 
		        }
		   
		        public void removeUpdate(DocumentEvent e){
		          pesquisarLista(pesquisa.getText());  
		        }

		        public void changedUpdate(DocumentEvent e){}
		     }
		    );   
	   }
	}
		
	private class OrdenarAction implements ActionListener{
		
		public void actionPerformed(ActionEvent event){

			Camara camara = new Camara();
			try {
				camara.obterDados();
			} catch (JAXBException | IOException e) {
				e.printStackTrace();
			}
			List<Deputado> deputados = camara.getDeputados();
			
			if(pesquisa.getText().equals("")){
				JOptionPane.showMessageDialog(null, "Digite o nome");
				pesquisa.requestFocus();
			}
			else{
				listaDeputados.clear();
				
				for(int i = 0; i<deputados.size(); i++){
					Deputado deputado = deputados.get(i);
					if(deputado.getNome().contains(pesquisa.getText().toUpperCase())){
						
						
						listaDeputados.addElement(deputado.getNome());
					}
				}
				if(listaDeputados.isEmpty()){
					JOptionPane.showMessageDialog(null, "Nenhum resultado encontrado");
					pesquisa.requestFocus();
				}
				contagem.setText(""+listaDeputados.getSize());
			}
		}
	}
	
	private class FiltragemAction implements ActionListener{
		
		public void actionPerformed(ActionEvent event){
			Camara camara = new Camara();
			try {
				camara.obterDados();
			} catch (JAXBException | IOException e) {
				e.printStackTrace();
			}
			
			List<Deputado> deputados = camara.getDeputados();
			
			if(lista.getSelectedIndex() == 1){
				listaDeputados.clear();
				for(int i = 0; i<deputados.size(); i++){;
					Deputado deputado = deputados.get(i);
					if(deputado.getCondicao().toUpperCase().equals ("Suplente")){
						listaDeputados.addElement(deputado.getNome());
					}
				}
				contagem.setText(""+listaDeputados.getSize());
			}
		}
	}
		
		
	private class InformacoesAction implements ActionListener{
		  
		public void actionPerformed(ActionEvent event){
			
			Camara camara = new Camara();
			try {
				camara.obterDados();
			} catch (JAXBException | IOException e) {
				e.printStackTrace();
			}
			List<Deputado> deputados = camara.getDeputados();
			
			if(lista.getSelectedValue() == informacoes){
				for(Deputado deputado : deputados){
				  linhainformacoes.add(new Object[]{deputado.getNome(),deputado.getPartido(),
						  deputado.getUf(), deputado.getEmail(), deputado.getCondicao()});
				
				  JOptionPane.showMessageDialog(null,"Informaçoes do Deputado" + lista
						  
						  
						  );
				  
				}
				  
			}
		}
	}

		public void pesquisarLista(String texto){
		    int pos = lista.getNextMatch(texto, 0, Position.Bias.Forward);
		    lista.setSelectedIndex(pos);
		    lista.ensureIndexIsVisible(pos);
	
		}
		

		private class PartidoAction implements ActionListener{
			
			public void actionPerformed(ActionEvent event){
				Camara camara = new Camara();
				try {
					camara.obterDados();
				} catch (JAXBException | IOException e) {
					e.printStackTrace();
				}
				
				List<Deputado> deputados = camara.getDeputados();
				
				if(lista2.getSelectedIndex() == 1){
					listaPartido.clear();
					for(int i = 0; i<deputados.size(); i++){;
						Deputado deputado = deputados.get(i);
						if(deputado.getPartido().toUpperCase().equals ("PDT")){
							listaPartido.addElement(deputado.getNome());
						}
					}
					
				}
			}
		}





}





	
	

	

